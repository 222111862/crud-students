package net.sytes.zeinhaddad.students.controller;

import java.util.List;
import net.sytes.zeinhaddad.students.dto.StudentDto;
import net.sytes.zeinhaddad.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller untuk halaman CRUD students.
 * @author zein
 */
@Controller
public class StudentController {
    @Autowired
    private StudentService studentService;

    /**
     * Halaman view CRUD students admin.
     * @param model
     * @return 
     */
    @GetMapping("/admin/students")
    public String index(Model model) {
        List<StudentDto> studentDtos = this.studentService.getStudents();
        model.addAttribute("students", studentDtos);
        return "/admin/students";
    }

    /**
     * Halaman form new student.
     * @param model
     * @return 
     */
    @GetMapping("/admin/students/create")
    public String create(Model model) {
        model.addAttribute("student", new StudentDto());
        return "/admin/students/create";
    }

    /**
     * Memproses request new student.
     * @param model
     * @param student
     * @return 
     */
    @RequestMapping(value = "/admin/students", method = RequestMethod.POST)
    public RedirectView store(Model model, @ModelAttribute("student") StudentDto student) {
        model.addAttribute("student", student);
        this.studentService.saveStudent(student);
        return new RedirectView("/admin/students");
    }

    /**
     * Halaman form edit student.
     * @param model
     * @param studentId
     * @return 
     */
    @GetMapping("/admin/students/edit/{studentId}")
    public String edit(Model model, @PathVariable Long studentId) {
        StudentDto student = this.studentService.getStudent(studentId);
        if (student == null) {
            return "/home";
        }

        model.addAttribute("student", student);
        return "/admin/students/edit";
    }

    /**
     * Memproses request edit student.
     * @param model
     * @param student
     * @return 
     */
    @RequestMapping(value = "/admin/students/edit", method = RequestMethod.POST)
    public RedirectView update(Model model, @ModelAttribute("student") StudentDto student) {
        model.addAttribute("student", student);
        this.studentService.updateStudent(student);
        return new RedirectView("/admin/students");
    }

    /**
     * Memproses request delete student.
     * @param studentId
     * @return 
     */
    @RequestMapping(value = "/admin/students/delete/{studentId}")
    public RedirectView destroy(@PathVariable Long studentId) {
        this.studentService.deleteStudent(studentId);
        return new RedirectView("/admin/students");
    }

    /**
     * Menampilkan view student tanpa C_UD.
     * @param model
     * @return 
     */
    @GetMapping("/students")
    public String viewStudents(Model model) {
        List<StudentDto> studentDtos = this.studentService.getStudents();
        model.addAttribute("students", studentDtos);
        return "/students";
    }

    /**
     * Halaman Home.
     * @return 
     */
    @GetMapping("/")
    public String home() {
        return "Home";
    }
}
