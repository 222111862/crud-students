package net.sytes.zeinhaddad.students.mapper;

import net.sytes.zeinhaddad.students.dto.StudentDto;
import net.sytes.zeinhaddad.students.entity.Student;

/**
 * Kelas untuk mengkonversi objek dari entitas ke bentuk dto-nya.
 * @author zein
 */
public class StudentMapper {
    /**
     * Konversi ke dto.
     * @param student
     * @return 
     */
    public static StudentDto mapToStudentDto(Student student) {
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .birthDate(student.getBirthDate())
                .createdOn(student.getCreatedOn())
                .updatedOn(student.getUpdatedOn())
                .build();

        return studentDto;
    }

    /**
     * Konversi ke entitas.
     * @param studentDto
     * @return 
     */
    public static Student mapToStudent(StudentDto studentDto) {
        Student student = Student.builder()
                .id(studentDto.getId())
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .birthDate(studentDto.getBirthDate())
                .createdOn(studentDto.getCreatedOn())
                .updatedOn(studentDto.getUpdatedOn())
                .build();

        return student;
    }
}
