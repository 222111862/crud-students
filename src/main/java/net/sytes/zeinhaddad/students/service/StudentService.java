package net.sytes.zeinhaddad.students.service;

import java.util.List;
import net.sytes.zeinhaddad.students.dto.StudentDto;

/**
 * Interface untuk melayani operasi CRUD students.
 * @author zein
 */
public interface StudentService {
    public List<StudentDto> getStudents();
    public StudentDto getStudent(Long studentId);
    public void updateStudent(StudentDto studentDto);
    public void deleteStudent(Long studentId);
    public void saveStudent(StudentDto studentDto);
}
