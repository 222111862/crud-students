package net.sytes.zeinhaddad.students.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import net.sytes.zeinhaddad.students.dto.StudentDto;
import net.sytes.zeinhaddad.students.entity.Student;
import net.sytes.zeinhaddad.students.mapper.StudentMapper;
import net.sytes.zeinhaddad.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Implementasi dari pelayanan CRUD table students.
 * @author zein
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;
    
    @Override
    public List<StudentDto> getStudents() {
        List<Student> students = this.studentRepository.findAll();
        List<StudentDto> studentDtos = students.stream()
                .map((student) -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());

        return studentDtos;
    }

    @Override
    public StudentDto getStudent(Long studentId) {
        Optional<Student> student = this.studentRepository.findById(studentId);
        if (student.isPresent()) {
            return StudentMapper.mapToStudentDto(student.get());
        } else {
            return null;
        }
    }

    @Override
    public void deleteStudent(Long studentId) {
        this.studentRepository.deleteById(studentId);
    }

    @Override
    public void updateStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }

    @Override
    public void saveStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }
}
